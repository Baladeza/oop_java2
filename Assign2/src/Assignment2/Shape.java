package Assignment2;

abstract class Shape {

	public abstract double Area();

	public abstract double Perimeter();
}

//circle

class Circle extends Shape {
	double ray;

	@Override
	public double Area() {
		double area = 3.14 * (ray * ray);
		return area;
	}

	@Override
	public double Perimeter() {
		double perimeter = 3.14 * (ray * 2);
		return perimeter;
	}

	public void setRay(double ray) {
		this.ray = ray;
	}

	public double getRay() {
		return ray;
	}
}

//square

class Square extends Shape {
	double length;

	@Override
	public double Area() {
		double area = length * length;
		return area;
	}

	@Override
	public double Perimeter() {
		double perimeter = length * 4;
		return perimeter;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getLength() {
		return length;
	}
}

class Rectangle extends Shape {
	double width;
	double height;

	@Override
	public double Area() {
		double area = width * height;
		return area;
	}

	@Override
	public double Perimeter() {
		double perimeter = width + height;
		return perimeter;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getHeight() {
		return height;
	}
}
