package Assignment2;

public class MainClass {

	public static void main(String[] args) {
		Circle circle = new Circle();
		circle.setRay(7);
		System.out.println("Area of circle is: " + circle.Area());
		System.out.println("Perimeter of circle is: " + circle.Perimeter());
		
		Square square = new Square();
		square.setLength(7);
		System.out.println("Area of square is: " + square.Area());
		System.out.println("Perimeter of square is: " + square.Perimeter());


		Rectangle rectangle = new Rectangle();
		rectangle.setHeight(5);
		rectangle.setWidth(6);
		System.out.println("Area of rectangle is: " + rectangle.Area());
		System.out.println("Perimeter of rectangle is: " + rectangle.Perimeter());
//----------------------------------------------------------------------------------
		Cats myCat = new Cats();
        Dog myDog = new Dog();
        myCat.Sound();
        myDog.Sound();
        
//----------------------------------------------------------------------------------
        StuA studentA =new StuA(12,44,89);
        System.out.println( studentA.getPercentage()+"%");
        StuB studentB =new StuB(10,23,77,88);
        System.out.println( studentB.getPercentage()+"%");
	}

}
