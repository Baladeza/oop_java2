package Assignment2;

public abstract class Animals {
	public abstract void Sound();
}

class Cats extends Animals {
	public void Sound() {
		System.out.println("Cats say: meow!");
	}
}

class Dog extends Animals {
	public void Sound() {
		System.out.println("Dogs say: woof!");
	}
}