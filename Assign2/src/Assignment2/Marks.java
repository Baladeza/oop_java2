package Assignment2;

public abstract class Marks {

	public abstract double getPercentage();
}

class StuA extends Marks {
	double mark;

	public StuA(double mathMarkA, double biologyMarkA, double chemistryMarkA) {

		this.mark = mathMarkA;
		this.mark = biologyMarkA;
		this.mark = chemistryMarkA;
	}

	public double getPercentage() {
		if (mark < 0 || mark > 100) {
			System.out.println("marks should be between 0 and 100.");
		}
		double[] allMarks = new double[3];
		allMarks[0] = mark;
		allMarks[1] = mark;
		allMarks[2] = mark;
		return allMarks;
	}

}

class StuB extends Marks {
	double mark;

	public StuB(double mathMarkB, double biologyMarkB, double chemistryMarkB, double historyyMarkB) {

		this.mark = mathMarkB;
		this.mark = biologyMarkB;
		this.mark = chemistryMarkB;
		this.mark = historyyMarkB;
	}

	public double getPercentage() {
		if (mark < 0 || mark > 100) {
			System.out.println("marks should be between 0 and 100.");
		}

		double[] allMarks = new double[4];
		allMarks[0] = mark;
		allMarks[1] = mark;
		allMarks[2] = mark;
		allMarks[3] = mark;
		return allMarks;
	}

}
