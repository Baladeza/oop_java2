package finalExam;

public class Message {
	protected User reciever;
	protected User sender;
	protected User body;

	public User getReciever() {
		return reciever;
	}

	public void setReciever(User reciever) {
		this.reciever = reciever;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getBody() {
		return body;
	}

	public void setBody(User body) {
		this.body = body;
	}

	public Message(User reciever, User sender, User body) {

		this.reciever = reciever;
		this.sender = sender;
		this.body = body;
	}

	@Override
	public String toString() {
		return "Message [reciever=" + reciever + ", sender=" + sender + ", body=" + body + "]";
	}

}
