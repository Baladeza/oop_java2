package finalExam;

public class SmsUser extends User {
	String phoneNumber;

	public SmsUser(String firstName, String lastName, Address address, String phoneNumber) {
		super(firstName, lastName, address);
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		///??if (phoneNumber.contains("\\d"))
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "SmsUser [phoneNumber=" + phoneNumber + "]";
	}

}
