package assignment8;

import java.io.*;
import java.util.ArrayList;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws IOException {




        ArrayList addressList = new ArrayList();
        addressList.add(new Address(65, "saad abad", "tehran", "tehran", "h6c1h2"));
        addressList.add(new Address(22, "alvand", "sari", "mazandaran", "768431"));
        addressList.add(new Address(43, "maziar", "gorgan", "golestan", "19987665"));
        addressList.add(new Address(654, "azadi", "tehran", "tehran", "h55f4556d"));
        addressList.add(new Address(789, "sarfaraz", "rasht", "gilan", "786765fg155"));

        writeToFile( addressList );
        ReadFromFile();


    }


        public static void writeToFile(ArrayList addressList ) throws IOException {
        try (BufferedWriter myWriter = new BufferedWriter(new FileWriter("src/Resource/text"))) {
           String list = addressList.toString();
            myWriter.write( list);
            myWriter.close();
        }catch (IOException e){
           throw e;
        }
    }
    public static void ReadFromFile( ) throws IOException {
        try (BufferedReader myReader = new BufferedReader(new FileReader("src/Resource/text"))) {
          Stream<String> content =  myReader.lines();
            content.forEach(row -> System.out.println(row));

        }catch (IOException e){
            throw e;
        }
    }
}
