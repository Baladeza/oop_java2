package assign5;

public class Dancer {
	String name;
	 int age;
	 
	
	// constructor
	public Dancer(String name, int age) {
		this.name = name;
		this.age = age;
		
	}
	
	//method
	public void Dance() {
	System.out.println(name + " is dancer and his/her age is " + age);
	}

	@Override
	public String toString() {
		return "Dancer [age=" + age + ", name=" + name + "]";
	}
	
}
