package assign5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		Dancer dancer = new Dancer("ali", 26);
		Dancer electricBoogieDancer = new ElectricBoogieDancer("reza", 19);
		Dancer breakdancer = new Breakdancer("moha", 22);

		ArrayList<Dancer> allDancer = new ArrayList();
		allDancer.add(dancer);
		allDancer.add(electricBoogieDancer);
		allDancer.add(breakdancer);

		for (Dancer item : allDancer) {
			item.Dance();
		}
	};

}
