package exam1;

public class Case {
	private String model;
	private String manufacture;
	private String PowerSupply;
	private Dimention dimention; 
	public Case(String model, String manufacture, String powerSupply, Dimention dimention) {
		super();
		this.model = model;
		this.manufacture = manufacture;
		PowerSupply = powerSupply;
		this.dimention = dimention;
	}
	
	public void pressPowerButton() {
		System.out.println("Power button pressed");
	}
	public String getModel() {
		return model;
	}
	public String getManufacture() {
		return manufacture;
	}
	public String getPowerSupply() {
		return PowerSupply;
	}
	public Dimention getDimention() {
		return dimention;
	}
	
}
