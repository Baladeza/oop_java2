package exam1;

public class Monitor {
	private String model;
	private String manufacture;
	private int size;
	private Resolution resolution;

	public String getModel() {
		return model;
	}

	public String getManufacture() {
		return manufacture;
	}

	public int getSize() {
		return size;
	}

	public Resolution getResolution() {
		return resolution;
	}

	public Monitor(String model, String manufacture, int size, Resolution resolution) {
		super();
		this.model = model;
		this.manufacture = manufacture;
		this.size = size;
		this.resolution = resolution;
	}
}
