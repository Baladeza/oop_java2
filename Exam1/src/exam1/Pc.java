package exam1;

public class Pc {
private Motherboard motherboard; 
private Monitor monitor;
private Case myCase;
public Pc(Motherboard motherboard, Monitor monitor, Case myCase) {
	
	this.motherboard = motherboard;
	this.monitor = monitor;
	this.myCase = myCase;
}

public Motherboard getMotherboard() {
	return motherboard;
}
public Monitor getMonitor() {
	return monitor;
}

public Case getMyCase() {
	return myCase;
}

}
