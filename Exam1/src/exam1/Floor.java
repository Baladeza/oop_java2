package exam1;

public class Floor {

	double width;
	double length;
	
	public Floor(double width, double length) {
		this.width = width;
		this.length = length;
		if (width < 0) {
			width = 0;
		}
		if (length < 0) {
			length = 0;
		}
	}
	
	public double getArea(){
		double area = width * length;
		return area;
	}

	
}
