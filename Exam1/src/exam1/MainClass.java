package exam1;

public class MainClass {

	public static void main(String[] args) {
		// Carpet
		Carpet carpet = new Carpet(3.5);
		Floor floor = new Floor(2.75, 4);
		Calculator calculator = new Calculator(floor, carpet);
		System.out.println("total = " + calculator.getTotalCost());
		carpet = new Carpet(1.5);
		floor = new Floor(5.4, 4.5);
		calculator = new Calculator(floor, carpet);
		System.out.println("total = " + calculator.getTotalCost());

		// Bank
		BankA bankA = new BankA();
		BankB bankB = new BankB();
		BankC bankC = new BankC();
		System.out.println(bankA.getDeposit() + bankB.getDeposit() + bankC.getDeposit());

		// PC
		Dimention dimention = new Dimention(20, 20, 5);
		Case myCase = new Case("220B", "Dell", "240", dimention);
		Monitor monitor = new Monitor("22inch", "Acer", 27, new Resolution(2540, 1440));
		Motherboard motherboard = new Motherboard("BJ-200", "asus", 4, 4, "v2.44");
		Pc myPc = new Pc(motherboard, monitor, myCase);
		myPc.getMyCase().pressPowerButton();
	}

}
