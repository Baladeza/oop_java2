package assignment7;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        //creating object from User
        User user1 = new User(55, "Jack", LocalDate.of(1998, 4,12));
        User user2 = new User(59, "Marry", LocalDate.of(1979, 5, 10));
        User user3 = new User(26, "Larry", LocalDate.of(1937, 6, 12));
        User user4 = new User(99, "Obrien", LocalDate.of(1968, 7, 25));
        User user5 = new User(43, "Ali", LocalDate.of(1952, 8, 4));

        // creating arrayList and putting objects into it
        List<User> usersArray = new ArrayList<User>();
        usersArray.add(user1);
        usersArray.add(user2);
        usersArray.add(user3);
        usersArray.add(user4);
        usersArray.add(user5);

       //Collections.sort( usersArray, new nameComprator());
        //Collections.sort( usersArray, new idComparator());
        Collections.sort( usersArray, new birthDateCompare());
       for(User item:usersArray){
           System.out.println(item);
       }
    }
}
