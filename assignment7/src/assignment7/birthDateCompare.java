package assignment7;

import java.util.Comparator;

public class birthDateCompare implements Comparator<User> {

    @Override
    public int compare(User o1, User o2) {
        return o1.getBirthdate().compareTo(o2.getBirthdate());
    }
}
