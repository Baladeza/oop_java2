package company;

public class CommissionEmploee extends Employee {

	protected double sales;
	protected double commission;

	public CommissionEmploee(String name, String ssn, double sales, double commission) {
		super(name, ssn);
		this.sales = sales;
		this.commission = commission;
	}

	public double getSales() {
		return sales;
	}

	public double getCommission() {
		return commission;
	}

	public double salary() {
		return getCommission() * getSales();
	}

	@Override
	public String toString() {
		return "CommissionEmploee [sales=" + sales + ", commission=" + commission + "]";
	}

}
