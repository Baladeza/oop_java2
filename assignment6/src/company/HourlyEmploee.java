package company;

public class HourlyEmploee extends Employee {
	protected double wage;
	protected double hours;

	public HourlyEmploee(String name, String ssn, double wage, double hours) {
		super(name, ssn);
		this.wage = wage;
		this.hours = hours;
	}

	public double getWage() {
		return wage;
	}

	public double getHours() {
		return hours;
	}

	public double salary() {
		return getWage() * getHours();
	}

	@Override
	public String toString() {
		return "HourlyEmploee [wage=" + wage + ", hours=" + hours + "]";
	}

}
