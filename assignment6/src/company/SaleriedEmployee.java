package company;

public class SaleriedEmployee extends Employee{
	protected double basicSalary;
	

	public SaleriedEmployee(String name, String ssn, double basicSalary) {
		super(name, ssn);
		this.basicSalary = basicSalary;
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public double salary() {
		return getBasicSalary();
	}

	@Override
	public String toString() {
		return "SaleriedEmployee [basicSalary=" + basicSalary + "]";
	}

	
}
