package company;

import java.util.ArrayList;

public class Company {

	public static void main(String[] args) {

		ArrayList<Employee> employee = new ArrayList<>();
		employee.add(new CommissionEmploee("ali","123-32", 1500.2,1.2));
		employee.add(new HourlyEmploee("reza","865-33", 290.5,18));
		employee.add(new SaleriedEmployee("moha","665-99", 2700.5));
		
		Payroll payroll = new Payroll(employee);
		
		payroll.paySalery();
	}

}
