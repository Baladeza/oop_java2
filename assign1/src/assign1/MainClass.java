package assign1;

public class MainClass {
	public static void main(String[] args) {

		// Person
		System.out.println("---------Person-----------------------");
		Person person = new Person();
		person.setFirstName("");
		person.setLastName("");
		person.setAge(10);
		System.out.println("fullname = " + person.getFullName());
		System.out.println(" teen = " + person.isTeen());
		person.setFirstName("john");
		person.setAge(18);
		System.out.println("fullName = " + person.getFullName());
		System.out.println(" teen = " + person.isTeen());
		person.setLastName("smith");
		System.out.println("fullName = " + person.getFullName());

		// SimpleCalculator
		System.out.println("---------SimpleCalculator-------------");

		SimpleCalculator calculator = new SimpleCalculator();
		calculator.setFirstNumber(5.0);
		calculator.setSecondNumber(4);
		System.out.println("add = " + calculator.getAdditionResult());
		System.out.println("subtrac = " + calculator.getSubtractionResult());
		calculator.setFirstNumber(5.25);
		calculator.setSecondNumber(0);
		System.out.println("multiply = " + calculator.getMultiplicationResult());
		System.out.println("devide = " + calculator.getDevisionResult());

		// Area
		System.out.println("-----------Wall Area-----------------");

		Wall wall = new Wall(5, 4);
		System.out.println("Area = " + wall.getArea());

		wall.setHeight(-1.5);

		System.out.println("Width= " + wall.getWidth());
		System.out.println("Height= " + wall.getHeight());
		System.out.println("Area= " + wall.getArea());
	}

}
