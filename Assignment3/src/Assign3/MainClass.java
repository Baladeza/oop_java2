package Assign3;

public class MainClass {

	public static void main(String[] args) {
		Rectangle rectangle = new Rectangle(5, 10);
        System.out.println("Rectangle width is: " + rectangle.getWidth());
        System.out.println("Rectangle length is: " + rectangle.getLength());
        System.out.println("Rectangle area is: " + rectangle.getArea());

        Cuboid cuboid = new Cuboid(5, 10, 5);
        System.out.println("Cuboid width is: " + cuboid.getWidth());
        System.out.println("Cuboid length is: " + cuboid.getLength());
        System.out.println("Cuboid area is: " + cuboid.getArea());
        System.out.println("Cuboid height is: " + cuboid.getHeight());
        System.out.println("Cuboid volume is :" + cuboid.getVolume());

	}

}
